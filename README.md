# Natural Colors

Palette of colors based on the the subjective experience of human perception (and not on color mixing). Based on a model which was first proposed by German physiologist Ewald Hering.

Available for:
- Affinity (.afpalette)
- System MacOS (.clr)
- Adobe (.ase)

![Illustration](./Natural%20Colors%20-%20V2_1200.jpg)
